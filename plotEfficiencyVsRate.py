#Load in modules and functions
import numpy as np
import pandas as pd
import argparse
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.transforms import blended_transform_factory
import os
import json

colors = ["firebrick", "steelblue", "seagreen", "coral", "darkorchid", "gray"]

efficiency_save = json.load(open("plots/efficiency.json"))
rate_save = json.load(open("plots/rate.json"))

fig, axes = plt.subplots(ncols=1, figsize=(6, 5))
ax = axes

rx = [i for i,j in enumerate(rate_save["x"]) if j>15 and j in efficiency_save["x"]]
ex = [i for i,j in enumerate(efficiency_save["x"]) if j>15]

c = 0
for key in efficiency_save:
    if key=="x":
        continue
    else:
        ax.errorbar(
            (np.array(rate_save[key])[rx]).tolist(),
            (np.array(efficiency_save[key][0])[ex]).tolist(),
            yerr = (np.array(efficiency_save[key][1])[ex]).tolist(),
            color = colors[c],
            label = key,
        )
        c += 1

ax.set_title("4J15 Trigger Efficiency vs. Rate")
ax.set_xlabel("Rate [kHz]")
ax.set_ylabel("True efficiency [%]")
ax.set_xscale('log')
ax.text(0.95, 0.95,"Inst. Luminosity: 2.0 $\\times$ ${10}^{34}$ cm${}^{-2}$ s${}^{-1}$",
     horizontalalignment="right",
     verticalalignment="center",
     transform = ax.transAxes)
ax.text(0.95, 0.91,"<$\mu$> $\leq$ 56.0",
     horizontalalignment="right",
     verticalalignment="center",
     transform = ax.transAxes)
ax.legend(bbox_to_anchor=[0.97, 0.90], loc="upper right")
fig.savefig("plots/efficiencyVsRate.png")
