#Load in modules and functions
import numpy as np
import pandas as pd
import argparse
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.transforms import blended_transform_factory
import os
import json

GEV = 1000
colors = ["firebrick", "steelblue", "seagreen", "coral", "darkorchid", "gray"]

test_samples = [
    "processed_JZ0.pkl",
    "processed_JZ1.pkl",
    "processed_JZ2.pkl",
    "processed_JZ3.pkl",
    "processed_JZ4.pkl",
]

#Arguments passed by the user
parser = argparse.ArgumentParser(description='Make efficiency plots')
parser.add_argument('--dir', dest='dir', type=str, default='samples', help='Directory to save processed file')
parser.add_argument('--trigger', dest='trigger', type=str, default='trig_L1_4J15', help='Trigger to compare')
parser.add_argument('--modelsToPlot', dest='models_to_plot', nargs="*", type=str, default=[], help='ML models to plot')
args = parser.parse_args()

print("Making efficiency plot")
if not os.path.exists("plots"):
    os.makedirs("plots")

#Get directory
dir = args.dir
dir = dir.splitlines()[0]

#Make full sample
print("-- Making sample...")
df = pd.read_pickle("{}/{}_{}".format(dir, args.trigger, test_samples[0]))
for i in test_samples[1:]:
    df = df.append(pd.read_pickle("{}/{}_{}".format(dir, args.trigger, i)), ignore_index=True)

#Just get events with 4 offline anti-kt R=0.4 jets with |eta|<2.5
df = df[(df.AntiKt4TruthJets_pt.map(lambda d: len(d)) >= 4)].copy()
df = df[(df.mu <= 56.0)].copy()

split_no = 100
df = df.sample(frac=1).reset_index(drop=True)
df_split = np.array_split(df, split_no)

#Make plot
print("-- Making plot...")
plot_name = "efficiency"
bins = np.array([0,2,5,10,15,20,30,50,75,100,150])

efficiency_save = {}

pass_all, edge_all = np.histogram(
    np.array([n[3]/GEV for n in df["AntiKt4TruthJets_pt"]]),bins,
    weights=df["weight"]
)

edge_dif = (edge_all[1:] - edge_all[:-1])/2
edge_all = (edge_all[1:] + edge_all[:-1])/2
efficiency_save["x"] = np.array(edge_all[pass_all>0]).tolist()

pass_4J15, _ = np.histogram(
    np.array([n[3]/GEV for n in df["AntiKt4TruthJets_pt"][df["trig_L1_4J15.0ETA25"]==1]]),bins,
    weights=df["weight"][df["trig_L1_4J15.0ETA25"]==1]
)
pass_4J15_errors = []
for nf in df_split:
    all, edge = np.histogram(
        np.array([n[3]/GEV for n in nf["AntiKt4TruthJets_pt"]]),bins,
        weights=nf["weight"]
    )
    edge = (edge[1:] + edge[:-1])/2
    passt, _ = np.histogram(
        np.array([n[3]/GEV for n in nf["AntiKt4TruthJets_pt"][nf["trig_L1_4J15.0ETA25"]==1]]),bins,
        weights=nf["weight"][nf["trig_L1_4J15.0ETA25"]==1]
    )
    pass_4J15_errors.append(passt[all>0]/all[all>0])
pass_4J15_errors = np.array([np.sqrt(np.mean(n**2))/np.sqrt(split_no) for n in np.transpose(pass_4J15_errors)])

pass_gBlock_wRho, _ = np.histogram(
    np.array([n[3]/GEV for n in df["AntiKt4TruthJets_pt"][df["trig_L1_4J15_wRho"]==1]]),bins,
    weights=df["weight"][df["trig_L1_4J15_wRho"]==1]
)
pass_gBlock_wRho_errors = []
for nf in df_split:
    all, edge = np.histogram(
        np.array([n[3]/GEV for n in nf["AntiKt4TruthJets_pt"]]),bins,
        weights=nf["weight"]
    )
    edge = (edge[1:] + edge[:-1])/2
    passt, _ = np.histogram(
        np.array([n[3]/GEV for n in nf["AntiKt4TruthJets_pt"][nf["trig_L1_4J15_wRho"]==1]]),bins,
        weights=nf["weight"][nf["trig_L1_4J15_wRho"]==1]
    )
    pass_gBlock_wRho_errors.append(passt[all>0]/all[all>0])
pass_gBlock_wRho_errors = np.array([np.sqrt(np.mean(n**2))/np.sqrt(split_no) for n in np.transpose(pass_gBlock_wRho_errors)])

fig, axes = plt.subplots(ncols=1, figsize=(6, 5))
ax = axes

ax.errorbar(edge_all[pass_all>0], pass_4J15[pass_all>0]/pass_all[pass_all>0], yerr=pass_4J15_errors,
    xerr=edge_dif[pass_all>0], marker="o", linestyle="-", capsize=2, color=colors[0], label="Run2 L1 4J15")
efficiency_save["Run2 L1 4J15"] = np.array([pass_4J15[pass_all>0]/pass_all[pass_all>0], pass_4J15_errors]).tolist()
ax.errorbar(edge_all[pass_all>0], pass_gBlock_wRho[pass_all>0]/pass_all[pass_all>0], yerr=pass_gBlock_wRho_errors,
    xerr=edge_dif[pass_all>0], marker="o", linestyle="-", capsize=2, color=colors[1], label="Raw sum w/ $\\rho$Asub")
efficiency_save["Raw sum w/ $\\rho$Asub"] = np.array([pass_gBlock_wRho[pass_all>0]/pass_all[pass_all>0], pass_gBlock_wRho_errors]).tolist()

for i,model in enumerate(args.models_to_plot):
    print("-- Now working on {} model...".format(model))

    pass_model, _ = np.histogram(
        np.array([n[3]/GEV for n in df["AntiKt4TruthJets_pt"][df["{}_{}".format(args.trigger, model)]==1]]),bins,
        weights=df["weight"][df["{}_{}".format(args.trigger, model)]==1]
    )
    pass_model_errors = []
    for nf in df_split:
        all, edge = np.histogram(
            np.array([n[3]/GEV for n in nf["AntiKt4TruthJets_pt"]]),bins,
            weights=nf["weight"]
        )
        edge = (edge[1:] + edge[:-1])/2
        passt, _ = np.histogram(
            np.array([n[3]/GEV for n in nf["AntiKt4TruthJets_pt"][nf["{}_{}".format(args.trigger, model)]==1]]),bins,
            weights=nf["weight"][nf["{}_{}".format(args.trigger, model)]==1]
        )
        pass_model_errors.append(passt[all>0]/all[all>0])
    pass_model_errors = np.array([np.sqrt(np.mean(n**2))/np.sqrt(split_no) for n in np.transpose(pass_model_errors)])

    ax.errorbar(edge_all[pass_all>0], pass_model[pass_all>0]/pass_all[pass_all>0], yerr=pass_model_errors,
        xerr=edge_dif[pass_all>0], marker="o", linestyle="-", capsize=2, color=colors[2+i], label=model)
    efficiency_save[model] = np.array([pass_model[pass_all>0]/pass_all[pass_all>0], pass_model_errors]).tolist()

    plot_name = plot_name + "__" + model

ax.set_title("Trigger Efficiencies")
ax.set_xlabel("4th leading truth jet $p_T$ [GeV]")
ax.set_ylabel("4J15 true efficiency [%]")
ax.axvline(x=15, color="black", linestyle="--")
tform = blended_transform_factory(ax.transData, ax.transAxes)
ax.text(14, 0.95,"15 GeV",
     horizontalalignment="right",
     verticalalignment="top",
     rotation="vertical",
     color="black",
     transform=tform)
ax.legend(loc="lower right")
fig.savefig("plots/{}.png".format(plot_name))

json.dump(efficiency_save, open("plots/efficiency.json", 'w'))

print("-- Finished!")
