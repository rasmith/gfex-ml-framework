#!/bin/bash
python preprocess_run.py -f JZ0.root -e 85000 --split_no 100 --xs 7.8420E+07 --eff 9.7550E-01 --nevents 850000 --dir /gpfs/slac/atlas/fs1/d/recsmith/gFEX
python preprocess_run.py -f JZ1.root -e 56000 --split_no 100 --xs 7.8420E+07 --eff 6.7152E-04 --nevents 56000  --dir /gpfs/slac/atlas/fs1/d/recsmith/gFEX
python preprocess_run.py -f JZ2.root -e 50000 --split_no 100 --xs 2.4332E+06 --eff 3.3434E-04 --nevents 50000  --dir /gpfs/slac/atlas/fs1/d/recsmith/gFEX
python preprocess_run.py -f JZ3.root -e 50000 --split_no 100 --xs 2.6454E+04 --eff 3.2012E-04 --nevents 50000  --dir /gpfs/slac/atlas/fs1/d/recsmith/gFEX
python preprocess_run.py -f JZ4.root -e 50000 --split_no 100 --xs 2.5463E+02 --eff 5.3137E-04 --nevents 50000  --dir /gpfs/slac/atlas/fs1/d/recsmith/gFEX
