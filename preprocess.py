#Load in modules and functions
import os
import pickle
import numpy as np
import uproot as upr
import pandas as pd
from math import isnan
import argparse

from scipy import spatial

DIST = 0.2
NUM = 9
GEV = 1000

#Arguments passed by the user
parser = argparse.ArgumentParser(description='Preprocess root files into HDF5 training and testing datasets')
parser.add_argument('-f', '--file', dest='ifile', type=str, default=None, help='Single pkl file to run on')
parser.add_argument('-e', '--event_num', dest='event_num', type=int, default=100, help='Number of events to save')
parser.add_argument('--xs', dest='xs', type=float, default=None, help='Xsec of sample')
parser.add_argument('--eff', dest='eff', type=float, default=None, help='Efficiency of sample')
parser.add_argument('--nevents', dest='nevents', type=float, default=None, help='Number of generated events of sample')
args = parser.parse_args()

#Open pkl file
with open(args.ifile, "rb") as fp:
    df = pickle.load(fp)

#Make gTower variables that actually make sense
df["gEmTowerEt"]   = np.array([np.concatenate((row[0:543],row[1088:1631]),axis=0) for row in df["gTowerEt"]])
df["gHadTowerEt"]  = np.array([np.concatenate((row[544:1087],row[1632:2175]),axis=0) for row in df["gTowerEt"]])

df["gTotTowerEt"]  = np.array([EM + HAD for EM,HAD in zip(df["gEmTowerEt"], df["gHadTowerEt"])])
df["gTotTowerPhi"] = np.array([np.concatenate((row[0:543],row[1088:1631]),axis=0) for row in df["gTowerPhi"]])
df["gTotTowerEta"] = np.array([np.concatenate((row[0:543],row[1088:1631]),axis=0) for row in df["gTowerEta"]])

#Only keep towers with |eta| < 2.4
df["gTotTowerEt"] = np.array([g[abs(eta)<2.4] for g,eta in zip(df["gTotTowerEt"],df["gTotTowerEta"])])
df["gTotTowerPhi"] = np.array([g[abs(eta)<2.4] for g,eta in zip(df["gTotTowerPhi"],df["gTotTowerEta"])])
df["gTotTowerEta"] = np.array([g[abs(eta)<2.4] for g,eta in zip(df["gTotTowerEta"],df["gTotTowerEta"])])

#Calculate estimation of rho for each event
print("Add rho...")
df["rho"] = np.array([np.mean(n[n<6*GEV]) for n in df["gTotTowerEt"]])

#Add weights to each event
print("Add weights...")
df["weight"] = df["eventWeight"]*args.xs*args.eff/args.nevents * args.nevents/args.event_num

#Find the closest towers surrounding the truth jets in the event and return their energy
print("Find gBlocks closest to truth jets...")
closestTowersEt = []
for ev in range(len(df["eventWeight"])):
    if ev%10==0: print("-- Processed {} of {} events".format(ev, len(df["eventWeight"])))
    gBlocks = []
    for jet in range(len(df["AntiKt4TruthJets_pt"][ev])):

        gCoords = np.array([(eta,phi) for eta,phi in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev])])
        gCoords = np.concatenate((gCoords, np.array([(eta,phi + 2*np.pi) for eta,phi in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev])])), axis=0)
        gCoords = np.concatenate((gCoords, np.array([(eta,phi - 2*np.pi) for eta,phi in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev])])), axis=0)

        gCoords_Et = np.array([(eta,phi,et) for eta,phi,et in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev],df["gTotTowerEt"][ev])])
        gCoords_Et = np.concatenate((gCoords_Et, np.array([(eta,phi + 2*np.pi,et) for eta,phi,et in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev],df["gTotTowerEt"][ev])])), axis=0)
        gCoords_Et = np.concatenate((gCoords_Et, np.array([(eta,phi - 2*np.pi,et) for eta,phi,et in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev],df["gTotTowerEt"][ev])])), axis=0)

        gCoords_tree = spatial.KDTree(gCoords)
        center_arg = (gCoords_tree.query([(df["AntiKt4TruthJets_eta"][ev][jet],df["AntiKt4TruthJets_phi"][ev][jet])]))[1][0]
        center_eta = gCoords[center_arg][0]
        center_phi = gCoords[center_arg][1]
        distance_check = np.sqrt(DIST**2 + DIST**2)

        closestCoords = (gCoords_tree.query([(center_eta,center_phi)], k=NUM, distance_upper_bound=distance_check))[1]
        closestCoords = closestCoords[closestCoords<len(gCoords)]
        closestCoords_Et = gCoords_Et[closestCoords]

        outputCoords_Et = ((np.array(sorted(closestCoords_Et, key=lambda k: [k[0],k[1]]))).T)[2]
        if len(outputCoords_Et) != NUM:
            outputCoords_Et = np.full((NUM,), np.nan)

        gBlocks.append(list(outputCoords_Et))

    closestTowersEt.append(gBlocks)
df["closestTowersEt"] = closestTowersEt
print("Finished finding gBlocks!")

#Find the closest towers surrounding the jFEX jets in the event and return their energy
print("Find gBlocks closest to jRoundJets...")
closestTowersEt_jFEX = []
for ev in range(len(df["eventWeight"])):
    if ev%10==0: print("-- Processed {} of {} events".format(ev, len(df["eventWeight"])))
    gBlocks = []
    for jet in range(len(df["jRoundJets_pt"][ev])):

        gCoords = np.array([(eta,phi) for eta,phi in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev])])
        gCoords = np.concatenate((gCoords, np.array([(eta,phi + 2*np.pi) for eta,phi in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev])])), axis=0)
        gCoords = np.concatenate((gCoords, np.array([(eta,phi - 2*np.pi) for eta,phi in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev])])), axis=0)

        gCoords_Et = np.array([(eta,phi,et) for eta,phi,et in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev],df["gTotTowerEt"][ev])])
        gCoords_Et = np.concatenate((gCoords_Et, np.array([(eta,phi + 2*np.pi,et) for eta,phi,et in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev],df["gTotTowerEt"][ev])])), axis=0)
        gCoords_Et = np.concatenate((gCoords_Et, np.array([(eta,phi - 2*np.pi,et) for eta,phi,et in zip(df["gTotTowerEta"][ev],df["gTotTowerPhi"][ev],df["gTotTowerEt"][ev])])), axis=0)

        gCoords_tree = spatial.KDTree(gCoords)
        center_arg = (gCoords_tree.query([(df["jRoundJets_eta"][ev][jet],df["jRoundJets_phi"][ev][jet])]))[1][0]
        center_eta = gCoords[center_arg][0]
        center_phi = gCoords[center_arg][1]
        distance_check = np.sqrt(DIST**2 + DIST**2)

        closestCoords = (gCoords_tree.query([(center_eta,center_phi)], k=NUM, distance_upper_bound=distance_check))[1]
        closestCoords = closestCoords[closestCoords<len(gCoords)]
        closestCoords_Et = gCoords_Et[closestCoords]

        outputCoords_Et = ((np.array(sorted(closestCoords_Et, key=lambda k: [k[0],k[1]]))).T)[2]
        if len(outputCoords_Et) != NUM:
            outputCoords_Et = np.full((NUM,), np.nan)

        gBlocks.append(list(outputCoords_Et))

    closestTowersEt_jFEX.append(gBlocks)
df["closestTowersEt_jFEX"] = closestTowersEt_jFEX
print("Finished finding gBlocks!")

#Turn python dictionary into a pandas dataframe
pd_df = pd.DataFrame().astype('object')
pd_df = pd_df.from_dict({k: df[k] for k in (
    "AntiKt4TruthJets_pt",
    "AntiKt4TruthJets_eta",
    "jRoundJets_pt",
    "jRoundJets_eta",
    "closestTowersEt",
    "closestTowersEt_jFEX",
    "jeta4_pt",
    "jeta4_eta",
    "hltjeta4_pt",
    "hltjeta4_eta",
    "Run2_L1Jet_Et",
    "Run2_L1Jet_eta",
    "mu",
    "rho",
    "eventWeight",
    "weight",
    "trig_L1_4J15.0ETA25",
    "trig_L1_XE50",
)})

#Only keep truth jets with nan NOT in closestTowersEt
pd_df["delJet"] = pd.Series([[isnan(gBlock[0]) for gBlock in row.closestTowersEt]
    for row in pd_df[["closestTowersEt"]].itertuples()], index=pd_df.index)
pd_df["closestTowersEt"] = pd.Series([[ct for ct,dj in zip(row.closestTowersEt, row.delJet) if dj==False]
    for row in pd_df[["closestTowersEt", "delJet"]].itertuples()], index=pd_df.index)
pd_df["AntiKt4TruthJets_pt"] = pd.Series([[ct for ct,dj in zip(row.AntiKt4TruthJets_pt, row.delJet) if dj==False]
    for row in pd_df[["AntiKt4TruthJets_pt", "delJet"]].itertuples()], index=pd_df.index)
pd_df["AntiKt4TruthJets_eta"] = pd.Series([[ct for ct,dj in zip(row.AntiKt4TruthJets_eta, row.delJet) if dj==False]
    for row in pd_df[["AntiKt4TruthJets_eta", "delJet"]].itertuples()], index=pd_df.index)
del pd_df['delJet']

#Only keep jFEX jets with nan NOT in closestTowersEt_jFEX
pd_df["delJet_jFEX"] = pd.Series([[isnan(gBlock[0]) for gBlock in row.closestTowersEt_jFEX]
    for row in pd_df[["closestTowersEt_jFEX"]].itertuples()], index=pd_df.index)
pd_df["closestTowersEt_jFEX"] = pd.Series([[ct for ct,dj in zip(row.closestTowersEt_jFEX, row.delJet_jFEX) if dj==False]
    for row in pd_df[["closestTowersEt_jFEX", "delJet_jFEX"]].itertuples()], index=pd_df.index)
pd_df["jRoundJets_pt"] = pd.Series([[ct for ct,dj in zip(row.jRoundJets_pt, row.delJet_jFEX) if dj==False]
    for row in pd_df[["jRoundJets_pt", "delJet_jFEX"]].itertuples()], index=pd_df.index)
pd_df["jRoundJets_eta"] = pd.Series([[ct for ct,dj in zip(row.jRoundJets_eta, row.delJet_jFEX) if dj==False]
    for row in pd_df[["jRoundJets_eta", "delJet_jFEX"]].itertuples()], index=pd_df.index)
del pd_df['delJet_jFEX']

#Save
print("Saving...")
file_name = args.ifile
file_name = file_name.replace("CHUNK","PROCESSED_CHUNK")
pd_df.to_pickle(file_name)

print("Finished processing root file!")
