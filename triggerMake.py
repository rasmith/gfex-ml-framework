#Load in modules and functions
import numpy as np
import pandas as pd
import argparse
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import os
import json
from keras.models import model_from_json

from sklearn.preprocessing import StandardScaler
from sklearn.externals.joblib import load

NUM = 9
GEV = 1000
height = int(np.sqrt(NUM))

#Arguments passed by the user
parser = argparse.ArgumentParser(description='Make trigger variables')
parser.add_argument('-f', '--file', dest='ifile', type=str, default=None, help='Single pickle file to run on')
args = parser.parse_args()

def ApplyModel(gBlocks, eta, rho, model, scaler):
    ApplyModel.counter += 1
    if ApplyModel.counter%10==0: print("{}/{}".format(ApplyModel.counter, len(df)))

    if len(gBlocks)<4:
        return (gBlocks, False)
    else:
        processed_gBlocks = []
        for i, gBlock in enumerate(gBlocks):
            gBlock = list((np.array(gBlock) - rho)/GEV)
            #Keep gBlocks with Et > 10 GeV after the area subtraction
            if sum(gBlock) < 10:
                processed_gBlocks.append(0)
            else:
                #X_test = (np.reshape(gBlock, (len(gBlock),1))).T
                #X_test = scaler.transform(X_test)
                #X_test = np.reshape(X_test, (height,height,1))
                X_test = np.append(gBlock, eta[i])
                X_test = np.reshape(X_test, (-1,1)).T
                X_test = scaler.transform(X_test)
                processed_gBlocks.append(model.predict(np.array(X_test))[0][0])

        return (processed_gBlocks, np.sort(processed_gBlocks)[::-1][3] > 15)

print("Add trigger variable to sample")
print("-- Opening sample...")
df = pd.read_pickle(args.ifile)

#Get the raw gBlocks and the area subtraction
df["gBlocksEt"] = pd.Series([[sum(towers)/GEV for towers in row.closestTowersEt_jFEX]
    for row in df[["closestTowersEt_jFEX"]].itertuples()], index=df.index)
df["gBlocksEt_wRho"] = pd.Series([list(np.array(row.gBlocksEt) - NUM*(row.rho)/GEV)
    for row in df[["gBlocksEt","rho"]].itertuples()], index=df.index)
df["trig_L1_4J15_raw"] = pd.Series([np.sort(row.gBlocksEt)[::-1][3] > 15
    if len(row.gBlocksEt)>=4 else False
    for row in df[["gBlocksEt"]].itertuples()], index=df.index)
df["trig_L1_4J15_wRho"] = pd.Series([np.sort(row.gBlocksEt_wRho)[::-1][3] > 15
    if len(row.gBlocksEt_wRho)>=4 else False
    for row in df[["gBlocksEt_wRho"]].itertuples()], index=df.index)

#Run over all the models currently in the "models" folder
for i,model in enumerate(os.listdir("models")):
    print("-- Now working on {} model...".format(model))

    #Load model
    json_file = open("models/{}/{}_architecture.json".format(model,model),"r")
    loaded_model_json = json_file.read()
    json_file.close()
    load_model = model_from_json(loaded_model_json)
    load_model.load_weights("models/{}/{}_weights.h5".format(model,model))
    load_model.compile(optimizer='adam', loss='mse')

    #Load scaling factor
    load_scaler = StandardScaler()
    load_scaler = load("models/{}/{}_scaler.bin".format(model,model))

    #Make new trigger variable
    ApplyModel.counter = 0
    df["gBlocksEt_{}".format(model)] = pd.Series([ApplyModel(row.closestTowersEt_jFEX, row.jRoundJets_eta, row.rho, load_model, load_scaler)[0]
        for row in df[["closestTowersEt_jFEX", "jRoundJets_eta", "rho"]].itertuples()], index=df.index)
    ApplyModel.counter = 0
    df["trig_L1_4J15_{}".format(model)] = pd.Series([ApplyModel(row.closestTowersEt_jFEX, row.jRoundJets_eta, row.rho, load_model, load_scaler)[1]
        for row in df[["closestTowersEt_jFEX", "jRoundJets_eta","rho"]].itertuples()], index=df.index)

#Save with new trigger variable
file_name = args.ifile
file_name = file_name.replace("CHUNK","PROCESSED_CHUNK")
df.to_pickle(file_name)
