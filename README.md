# gFEX ML Framework

## Goal
Use machine learning to improve jet energy estimation for the Level-1 calorimeter trigger using the gFEX (Global Feature Extractor) system.

## Background
The LHC is going to have a considerable increase in luminosity following the second long shutdown and the Phase-1 upgrade. The current L1Calo system will need to be upgraded to maintain high trigger acceptance and low trigger rate in the harsher Run 3 environment and beyond. gFEX is one of many Level 1 jet trigger modules being developed for the Phase-1 upgrade. gFEX "sees" the entire calorimeter (EM + Had) in one module, segmented into "gTowers" with 0.2x0.2 resolution. Thus algorithms can be used to scan the entire range of the calorimeter to perform jet finding, pileup suppression, and energy estimation, among other calculations.

## How does it work?
First, this project assumes the use of the jFEX jet-finding algorithm i.e. the locations of the jFEX "jRoundJets" are used to base the location of the gBlock in eta/phi space. A "gBlock" is then defined as a 3x3 grid of gTowers centered at the location of the jFEX jet. The energies of these gTowers are then used to make an estimation of the energy of the anti-kT 0.4 truth jet that passed through this location. This estimation can then be used to evaluate trigger performance.

### Preprocessing
Find the gBlocks associated with the anti-kT 0.4 truth jets in the event. Ignore the forward region and jets that do not have an associated 3x3 gBlock. "Rho" is calculated as the mean energy of the gTowers with < 6 GeV Et per event. Weights are also calculated per event. Split the events into training and testing samples and save as `.pkl` files.
```
python preprocess_run.py -f JZ0.root -e 85000 --split_no 100 --xs 7.8420E+07 --eff 9.7550E-01 --nevents 850000 --dir /gpfs/slac/atlas/fs1/d/recsmith/gFEX
```
To preprocess everything at once run `preprocess_all.sh`.

### Training
Design the machine learning models to be created and trained in `modelDesign.py`. The model architecture and weights, as well as some evaluative plots, will be saved under the `model_name` in `models`.
```
python modelDesign.py
```

### Triggers
Run over all the models in `models`, do energy estimation, and check whether the events passed the trigger. For now only comparing against the 4J15 trigger. Save the energy estimation and the trigger pass/fail in new `.pkl` files in `samples`.
```
python triggerMake_run.py -f processed_JZ0.pkl --split_no 100 --dir /gpfs/slac/atlas/fs1/d/recsmith/gFEX
```
To process the trigger for all samples run `triggerMake_all.sh`.

### Making plots
Make plots to evaluate trigger efficiency and make rate calculations. The "Run2 L1 4J15" efficiency/rates in the following plots are from the current L1 trigger, which uses 4x4 towers 0.1 resolution as RoI's and, within them, 2x2 trigger tower cores as jet trigger elements.

```
python plotEfficiency.py --dir /gpfs/slac/atlas/fs1/d/recsmith/gFEX --modelsToPlot Fully_Connected_1HL Fully_Connected_2HL Fully_Connected_3HL
```
![efficiency_plot](plots/efficiency__Fully_Connected_1HL__Fully_Connected_2HL__Fully_Connected_3HL.png)

```
python plotRateCalculation.py --dir /gpfs/slac/atlas/fs1/d/recsmith/gFEX --modelsToPlot Fully_Connected_1HL Fully_Connected_2HL Fully_Connected_3HL
```
![reco_plot](plots/recoRate__Fully_Connected_1HL__Fully_Connected_2HL__Fully_Connected_3HL.png)

```
python plotEfficiencyVsRate.py
```
![efficiency_vs_rate](plots/efficiencyVsRate.png)
