import numpy as np
from keras.models import Sequential, Model, load_model
from keras.layers import Masking, Input, Dense, Dropout, GRU, LSTM, Flatten, AveragePooling2D
from keras.layers import concatenate, BatchNormalization, Conv2D, Activation, MaxPooling2D, Deconvolution2D
import keras.backend as K

from modelTrain import ModelTrain

NUM = 9
height = int(np.sqrt(NUM))

def customLoss(y_true, y_pred):
    diff = K.pow(K.abs((y_true - y_pred) / K.clip(K.abs(y_true),
                                            K.epsilon(),
                                            None)),4)
    return 100. * K.mean(diff, axis=-1)

################################################################################
##                               LINEAR MODEL                                 ##
################################################################################

model_name = "Linear_0HL"

model = Sequential()
model.add(Dense(1, input_shape=(NUM+1,)))
#model.add(Flatten(input_shape=(NUM+1,1)))
#model.add(Dense(1))

print("Creating and compiling model: {}".format(model_name))
model.compile(optimizer='adam', loss='mse', metrics=['mse'])
ModelTrain(model, model_name, epos=30, bsize=256, pati=10, make_plots=True)

################################################################################
##                  FULLY CONNECTED MODEL - 1 hidden layer                    ##
################################################################################

model_name = "Fully_Connected_1HL"

model = Sequential()
#model.add(Flatten(input_shape=(NUM+1,1)))
model.add(Dense(100, activation="relu", input_shape=(NUM+1,)))
model.add(Dense(1))

print("Creating and compiling model: {}".format(model_name))
model.compile(optimizer='adam', loss='mse', metrics=['mse'])
ModelTrain(model, model_name, epos=30, bsize=256, pati=10, make_plots=True)

################################################################################
##                  FULLY CONNECTED MODEL - 2 hidden layers                   ##
################################################################################

model_name = "Fully_Connected_2HL"

model = Sequential()
#model.add(Flatten(input_shape=(NUM+1,1)))
model.add(Dense(100, activation="relu", input_shape=(NUM+1,)))
model.add(Dense(100, activation="relu"))
model.add(Dense(1))

print("Creating and compiling model: {}".format(model_name))
model.compile(optimizer='adam', loss='mse', metrics=['mse'])
ModelTrain(model, model_name, epos=30, bsize=256, pati=10, make_plots=True)

################################################################################
##                  FULLY CONNECTED MODEL - 3 hidden layers                   ##
################################################################################

model_name = "Fully_Connected_3HL"

model = Sequential()
#model.add(Flatten(input_shape=(NUM+1,1)))
model.add(Dense(100, activation="relu", input_shape=(NUM+1,)))
model.add(Dense(100, activation="relu"))
model.add(Dense(100, activation="relu"))
model.add(Dense(1))

print("Creating and compiling model: {}".format(model_name))
model.compile(optimizer='adam', loss='mse', metrics=['mse'])
ModelTrain(model, model_name, epos=30, bsize=256, pati=10, make_plots=True)

################################################################################
##                                CNN MODEL                                   ##
################################################################################
"""
model_name = "CNN"

model = Sequential()

model.add(Conv2D(16, (3, 3), strides=(1, 1), padding="same", input_shape=(height,height,1)))
#model.add(BatchNormalization())
model.add(Activation('relu'))

model.add(Conv2D(32, (3, 3), strides=(1, 1), padding="same"))
#model.add(BatchNormalization())
model.add(Activation('relu'))

model.add(Conv2D(64, (3, 3), strides=(1, 1), padding="same"))
#model.add(BatchNormalization())
model.add(Activation('relu'))

model.add(Flatten())

model.add(Dense(512))
#model.add(BatchNormalization())
model.add(Activation('relu'))

model.add(Dense(512))
#model.add(BatchNormalization())
model.add(Activation('relu'))

model.add(Dense(1))

print("Creating and compiling model: {}".format(model_name))
model.compile(optimizer='adam', loss='mean_absolute_percentage_error', metrics=['mean_absolute_percentage_error'])
ModelTrain(model, model_name, epos=100, bsize=1024, pati=10, make_plots=True)
"""
################################################################################
##                                                                            ##
################################################################################
