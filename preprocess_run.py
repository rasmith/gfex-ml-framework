#Load in modules and functions
import numpy as np
import pandas as pd
import uproot as upr
import argparse

import os
import subprocess
import time
import shutil
import pickle

#Arguments passed by the user
parser = argparse.ArgumentParser(description='Split input file, make trigger variables, and then put back together')
parser.add_argument('-f', '--file', dest='ifile', type=str, default=None, help='Single root file to run on')
parser.add_argument('-e', '--event_num', dest='event_num', type=int, default=100, help='Number of events to save')
parser.add_argument('--dir', dest='dir', type=str, default='samples', help='Directory to save processed file')
parser.add_argument('--split_no', dest='split_no', type=int, default=None, help='Split dataframe into # chunks')
parser.add_argument('--xs', dest='xs', type=float, default=None, help='Xsec of sample')
parser.add_argument('--eff', dest='eff', type=float, default=None, help='Efficiency of sample')
parser.add_argument('--nevents', dest='nevents', type=float, default=None, help='Number of generated events of sample')
args = parser.parse_args()

print("Preprocess the sample '{}'".format(args.ifile))

#Get directory
dir = args.dir
dir = dir.splitlines()[0]

#Make the workspace
print("-- Making workspace...")
if not os.path.exists("workspace"):
    os.makedirs("workspace")

#Open root file
vars_to_open = [
    "gTower*",
    "AntiKt4TruthJets*",
    "Run2_L1Jet*",
    "jRoundJets*",
    "jeta4*",
    "hltjeta4*",
    "mu",
    "trig_L1_4J15.0ETA25",
    "trig_L1_XE50",
    "eventWeight",
]
file = upr.open("{}/{}".format(dir, args.ifile))["ntuple"]

file_name = args.ifile
file_name = file_name.replace(".root","")

#Split dataframe into chunks
print("-- Splitting sample into {} chunks...".format(args.split_no))
df_chunks = np.array_split(np.array(range(args.event_num)), args.split_no)
for i,chunk in enumerate(df_chunks):

    #Get arrays
    df = file.arrays(vars_to_open, namedecode="utf-8", entrystart=chunk[0] , entrystop=chunk[-1]+1)

    #Save chunks in workspace
    with open("workspace/{}_CHUNK_{}.pkl".format(file_name, i), "wb") as fp:
        pickle.dump(df, fp, protocol=pickle.HIGHEST_PROTOCOL)

    #Submit chunks to queue
    print("Submiting CHUNK #{}".format(i))
    os.system("LSB_JOB_REPORT_MAIL=N bsub -N -W 50:00 -q atlas-t3 'python preprocess.py -f workspace/{}_CHUNK_{}.pkl \
    -e {} \
    --xs {} \
    --eff {} \
    --nevents {} '".format(file_name, i, args.event_num, args.xs, args.eff, args.nevents))

#Check if the jobs are done
not_done = True
while not_done:
    time.sleep(10) #Wait 10 seconds
    result = subprocess.Popen(["bjobs"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    result = (result.stdout.read()).decode("utf-8")
    os.system("bjobs")
    #Continue once all the jobs are done
    if "No unfinished job found" in result:
        not_done = False

#Put chunks back together
print("-- Putting chunks back together...")
new_df = pd.read_pickle("workspace/{}_PROCESSED_CHUNK_{}.pkl".format(file_name, 0))
for i in range(args.split_no)[1:]:
    if i%10==0: print("Adding PROCESSED_CHUNK {}".format(i))
    new_df = new_df.append(pd.read_pickle("workspace/{}_PROCESSED_CHUNK_{}.pkl".format(file_name, i)), ignore_index=True)

print("-- Save newly processed file...")
new_df.to_pickle("{}/processed_{}.pkl".format(dir, file_name))

print("-- Deleting workspace...")
shutil.rmtree("workspace")
#os.remove("log.log")

print("-- Finished!")
