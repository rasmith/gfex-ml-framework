#Load in modules and functions
import os
import json
import shutil
import numpy as np
import pandas as pd
import argparse
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from keras.callbacks import EarlyStopping

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.externals.joblib import dump

from scipy.optimize import curve_fit

dir = "/gpfs/slac/atlas/fs1/d/recsmith/gFEX"

GEV = 1000
NUM = 9

# Bukin ######################################################################################
def bukin(inp, Ap, Xp, sigp, xi, rho1, rho2):

    #Ap = 2.2
    #Xp = 0
    #sigp = 0.15

    sigp = abs(sigp)

    consts = 2*np.sqrt(2*np.log(2))

    r1,r2,r3,r4,r5,hp = 0,0,0,0,0,0
    x1,x2 = 0,0
    P = []

    hp = sigp*consts
    r3=np.log(2)
    r4=np.sqrt((xi**2)+1)
    r1=xi/r4

    if (abs(xi) > np.exp(-6)):
        r5 = xi/np.log(r4+xi)
    else:
        r5 = 1

    x1 = Xp + (hp / 2) * (r1-1)
    x2 = Xp + (hp / 2) * (r1+1)

    for x in inp:
        #Left Side
        if (x < x1):
            r2=rho1*(((x-x1)/(Xp-x1))**2)-r3 + 4 * r3 * (x-x1)/hp * r5 * r4/((r4-xi)**2)

        #Center
        elif (x < x2):
            if (abs(xi) > np.exp(-6)):
                r2=np.log(1 + 4 * xi * r4 * (x-Xp)/hp)/np.log(1+2*xi*(xi-r4))
                r2=-r3*(r2**2)
            else:
                r2=-4*r3*(((x-Xp)/hp)**2)

        #Right Side
        else:
            r2=rho2*(((x-x2)/(Xp-x2))**2)-r3 - 4 * r3 * (x-x2)/hp * r5 * r4/((r4+xi)**2)

        fit_result = 0
        if (abs(r2) > 100):
            fit_result = 0
        else:
            fit_result = Ap*np.exp(r2)

        P.append(fit_result)

    return P
# Bukin ######################################################################################

#Make folder for models
if not os.path.exists("models"):
    os.makedirs("models")

train_samples = [
    #"processed_JZ0.pkl", #Don't train on minbias
    "processed_JZ1.pkl",
    "processed_JZ2.pkl",
    "processed_JZ3.pkl",
    "processed_JZ4.pkl",
]

def ModelTrain(model, model_name, epos=100, bsize=256, pati=10, make_plots=True):
    #Make folder for this specific model, overwrite if it already exists
    if os.path.exists("models/{}".format(model_name)):
        shutil.rmtree("models/{}".format(model_name))
    os.makedirs("models/{}".format(model_name))

    #Flatten dataframe from event level to jet level.
    def flatten(f, key):
        if (key!="closestTowersEt") and ("Truth" not in key):
            return [k for k,jetlist in zip(f[key],f["AntiKt4TruthJets_pt"]) for jet in jetlist]
        else:
            return [jet for jetlist in f[key] for jet in jetlist]

    print("-- Making inputs for model")
    #Make training sample and flatten to jet level
    df_train = pd.read_pickle("{}/{}".format(dir,train_samples[0]))
    for i in train_samples[1:]:
        df_train = df_train.append(pd.read_pickle("{}/{}".format(dir,i)), ignore_index=True)
    df_train = df_train[["closestTowersEt", "AntiKt4TruthJets_pt", "AntiKt4TruthJets_eta", "rho", "weight"]]
    df_train = pd.DataFrame({key:flatten(df_train, key) for key,c in df_train.iteritems()})
    df_train = df_train.reset_index(drop=True)

    #Do the area subtraction (rho)
    df_train["closestTowersEt"] = pd.Series([list(np.array(row.closestTowersEt) - row.rho)
        for row in df_train[["closestTowersEt","rho"]].itertuples()], index=df_train.index)
    #Keep gBlocks with Et > 10 GeV after the area subtraction
    df_train = df_train[(df_train.closestTowersEt.map(lambda d: sum(d)) > 10*GEV)].copy()

    df_train = df_train[df_train.AntiKt4TruthJets_pt < 100*GEV].copy()
    df_train = df_train.reset_index(drop=True)

    #Make inputs for network
    X = []
    for i in range(NUM):
        X.append(np.array([n[i] for n in df_train["closestTowersEt"].values])/GEV)
    X = np.array(X).T
    X = np.array([np.append(x,eta) for x,eta in zip(X, df_train["AntiKt4TruthJets_eta"])])
    print(np.shape(X))
    print(np.shape(X[0]))
    Y = (df_train["AntiKt4TruthJets_pt"].values)/GEV

    """
    #Reweight by pT
    start, stop, step = 0, 3000*GEV, 0.1*GEV
    pT_edges = np.arange(start, stop+step, step)

    #Get the bin that each entry corresponds to
    x_ind = np.digitize(df_train["AntiKt4TruthJets_pt"], pT_edges) - 1

    #Make the histogram
    hist, _ = np.histogram(df_train["AntiKt4TruthJets_pt"], bins=pT_edges, weights=df_train["weight"])

    #Normalize
    epsilon = 1e-8
    hist = hist / np.sum(hist) + epsilon

    #Reweight the jets
    df_train["new_weight"] = pd.Series([hist[ix] for ix in x_ind])
    """
    W = df_train["weight"].values

    #Split into train and test sets
    ix = range(np.shape(X)[0])
    X_train, X_test, Y_train, Y_test, W_train, W_test, ix_train, ix_test = train_test_split(
        X, Y, W, ix, train_size=0.8, test_size=0.2,
    )
    X_test_old = X_test
    print("-- Training on {} inputs".format(len(X_train)))

    #Scale input and save scaler for future datasets
    print("-- Scaling")
    #X_flat = (X_train.flatten()).reshape(-1,1)
    #scaler = StandardScaler()
    #scaler.fit(X_flat)
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)
    X_test  = scaler.transform(X_test)
    dump(scaler, "models/{}/{}_scaler.bin".format(model_name,model_name), compress=True)
    """
    #Reshape to be NxN pixels
    height = int(np.sqrt(NUM))
    X_train = np.array([np.reshape(n, (height,height,1)) for n in X_train])
    X_test  = np.array([np.reshape(n, (height,height,1)) for n in X_test])
    """

    #Train model
    print("-- Training")
    try:
        history = model.fit(
            X_train, Y_train,
            callbacks = [
                EarlyStopping(verbose=True, patience=pati, monitor='val_loss'),
            ],
            epochs=epos, batch_size=bsize,
            validation_split = 0.1,
            verbose = True,
            sample_weight = W_train
    )
    except KeyboardInterrupt:
        print ("-- Training ended early")

    print("-- Saving model architecture")
    with open("models/{}/{}_architecture.json".format(model_name,model_name),"w") as arch_file:
        arch_file.write(model.to_json())

    print("-- Saving model weights")
    model.save_weights("models/{}/{}_weights.h5".format(model_name,model_name))

    print("-- Training complete!")

    if not make_plots:
        print("-- Not making plots. Finished!")
    else:
        print("-- Making plots")

        #Make prediction
        Y_pred = np.array([i[0] for i in model.predict(X_test)])

        #Make regression resolution plot
        fig = plt.figure(figsize=(5, 4), dpi=100)
        ax = fig.add_subplot(111)
        im = ax.hist2d(
            Y_test,
            (Y_test-Y_pred)/Y_test,
            [np.linspace(0,100,51),np.linspace(-5,1.5,51)],
            norm=colors.LogNorm(),
            cmap=plt.cm.BuPu,
            weights=df_train["weight"][ix_test],
        )
        avg1 = np.array([np.ma.average(im[2][:-1]+(im[2][1]-im[2][0])/2, weights=pop) for pop in im[0]])
        std1 = np.array([np.sqrt(np.ma.average((im[2][:-1]+(im[2][1]-im[2][0])/2-avg)**2,weights=pop)) for avg,pop in zip(avg1,im[0])])
        ax.errorbar(im[1][:-1]+(im[1][1]-im[1][0])/2, avg1, yerr = std1, color="yellow", markersize=3, fmt='o')
        ax.axhline(y=0, color="black")
        ax.set_title("Model: {}".format(model_name))
        ax.set_xlabel("${p_T}^{\t{truthjet}}$ [GeV]")
        ax.set_ylabel("$\dfrac{{p_T}^{\t{truthjet}} - {E_T}^{\t{gFEX}}}{{p_T}^{\t{truthjet}}}$")
        fig.colorbar(im[3],ax=ax)
        fig.tight_layout()
        fig.savefig("models/{}/{}_RegressionResolution.png".format(model_name,model_name))

        #Make regression difference plot
        fig = plt.figure(figsize=(5, 4), dpi=100)
        ax = fig.add_subplot(111)
        im = ax.hist2d(
            Y_test,
            Y_test-Y_pred,
            [np.linspace(0,100,51),np.linspace(-100,100,51)],
            norm=colors.LogNorm(),
            cmap=plt.cm.BuPu,
            weights=df_train["weight"][ix_test],
        )
        avg1 = np.array([np.ma.average(im[2][:-1]+(im[2][1]-im[2][0])/2, weights=pop) for pop in im[0]])
        std1 = np.array([np.sqrt(np.ma.average((im[2][:-1]+(im[2][1]-im[2][0])/2-avg)**2,weights=pop)) for avg,pop in zip(avg1,im[0])])
        ax.errorbar(im[1][:-1]+(im[1][1]-im[1][0])/2, avg1, yerr = std1, color="yellow", markersize=3, fmt='o')
        ax.axhline(y=0, color="black")
        ax.set_title("Model: {}".format(model_name))
        ax.set_xlabel("${p_T}^{\t{truthjet}}$ [GeV]")
        ax.set_ylabel("${p_T}^{\t{truthjet}} - {E_T}^{\t{gFEX}}$ [GeV]")
        fig.colorbar(im[3],ax=ax)
        fig.tight_layout()
        fig.savefig("models/{}/{}_RegressionDifference.png".format(model_name,model_name))

        #Make bukin plot
        fig = plt.figure(figsize=(5, 4), dpi=100)
        ax = fig.add_subplot(111)

        raw_hist = ax.hist(
            (Y_test - np.array([sum(i) for i in X_test_old]))/Y_test,
            np.linspace(-2,2,50),
            histtype="step",
            color="orange",
            weights=df_train["weight"][ix_test],
            density=True,
        )

        cnn_hist = ax.hist(
            (Y_test - Y_pred)/Y_test,
            np.linspace(-2,2,50),
            histtype="step",
            color="green",
            weights=df_train["weight"][ix_test],
            density=True,
        )

        raw_data,raw_bins = raw_hist[0],raw_hist[1]
        raw_bincenters = np.array([0.5 * (raw_bins[i] + raw_bins[i+1]) for i in range(len(raw_bins)-1)])
        raw_popt, raw_pcov = curve_fit(bukin, xdata=raw_bincenters, ydata=raw_data, maxfev=5000, p0=[1,0.5,0.2,0.4,0.01,-3])
        #print(raw_popt)
        raw_xspace = np.linspace(-2, 2, 1000)
        ax.plot(
            raw_xspace,
            bukin(raw_xspace,*raw_popt),
            color="orange",
            alpha=0.5,
            label="Raw Sum \n $\mu_p$ = {} \n $\sigma_p$ = {}".format(round(raw_popt[1],4),round(raw_popt[2],4))
        )

        cnn_data,cnn_bins = cnn_hist[0],cnn_hist[1]
        cnn_bincenters = np.array([0.5 * (cnn_bins[i] + cnn_bins[i+1]) for i in range(len(cnn_bins)-1)])
        cnn_popt, cnn_pcov = curve_fit(bukin, xdata=cnn_bincenters, ydata=cnn_data, maxfev=5000, p0=[1,0,0.15,-0.07,0.1,-0.3])
        #print(cnn_popt)
        cnn_xspace = np.linspace(-2, 2, 1000)
        ax.plot(
            cnn_xspace,
            bukin(cnn_xspace,*cnn_popt),
            color="green",
            alpha=0.5,
            label="After model \n $\mu_p$ = {} \n $\sigma_p$ = {}".format(round(cnn_popt[1],4),round(cnn_popt[2],4))
        )

        ax.set_title("Model: {}".format(model_name))
        ax.set_xlabel("$\dfrac{{p_T}^{\t{truthjet}} - {E_T}^{\t{gFEX}}}{{p_T}^{\t{truthjet}}}$")
        ax.set_ylabel("AU")
        ax.legend(loc="upper left", frameon=False)
        fig.tight_layout()
        fig.savefig("models/{}/{}_BukinPlot.png".format(model_name,model_name))

        print("-- Finished making plots!")
