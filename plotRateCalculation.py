#Load in modules and functions
import numpy as np
import pandas as pd
import argparse
import os
import json
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.transforms import blended_transform_factory

GEV = 1000
luminosity = 0.02 #Instantaneous luminosity 2.0 x 10^34 cm-2 s-1 => kHz
luminosity *= 1/89.0 #Match up to public plots
colors = ["firebrick", "steelblue", "seagreen", "coral", "darkorchid", "gray"]

test_samples = [
    "processed_JZ0.pkl",
    "processed_JZ1.pkl",
    "processed_JZ2.pkl",
    "processed_JZ3.pkl",
    "processed_JZ4.pkl",
]

#Arguments passed by the user
parser = argparse.ArgumentParser(description='Preprocess root files into HDF5 training and testing datasets')
parser.add_argument('--dir', dest='dir', type=str, default='samples', help='Directory to save processed file')
parser.add_argument('--trigger', dest='trigger', type=str, default='trig_L1_4J15', help='Trigger to compare')
parser.add_argument('--modelsToPlot', dest='models_to_plot', nargs="*", type=str, default=[], help='ML models to plot')
args = parser.parse_args()

print("Making rate plots")
if not os.path.exists("plots"):
    os.makedirs("plots")

#Get directory
dir = args.dir
dir = dir.splitlines()[0]

#Make full sample
print("-- Making sample...")
df = pd.read_pickle("{}/{}_{}".format(dir, args.trigger, test_samples[0]))
for i in test_samples[1:]:
    df = df.append(pd.read_pickle("{}/{}_{}".format(dir, args.trigger, i)), ignore_index=True)

#Just get events with <mu> <=56
df = df[(df.mu <= 56.0)].copy()

#Make reco rate plot
print("-- Making reco plot...")
plot_name = "recoRate"
hist_bins = np.array([0,2,5,10,15,20,30,50,75,100,150])

rate_save = {}

fig, ax = plt.subplots(ncols=1, figsize=(6, 5))

"""
hist, edge, _ = ax.hist(np.array([n[3]/GEV for n in df["AntiKt4TruthJets_pt"] if (len(n)>=4) and (n[3]>=15*GEV)]),
         weights  = np.array([weight*luminosity for weight,n in zip(df["weight"],df["AntiKt4TruthJets_pt"]) if (len(n)>=4) and (n[3]>=15*GEV)]),
         bins     = hist_bins,
         histtype = "step",
         label    = "Truth",
         color    = "black",
         cumulative = -1,
)
"""

hist, edge, _ = ax.hist(np.array([(np.sort(pt[abs(eta)<2.5])[::-1])[3]/GEV
    for pt, eta in zip(df["Run2_L1Jet_Et"][df["trig_L1_4J15.0ETA25"]==1], df["Run2_L1Jet_eta"][df["trig_L1_4J15.0ETA25"]==1])
    if len(pt[abs(eta)<2.5])>=4]),
         weights  = np.array([ weight*luminosity
             for pt, eta, weight in zip(df["Run2_L1Jet_Et"][df["trig_L1_4J15.0ETA25"]==1], df["Run2_L1Jet_eta"][df["trig_L1_4J15.0ETA25"]==1], df["weight"][df["trig_L1_4J15.0ETA25"]==1])
             if len(pt[abs(eta)<2.5])>=4]),
         bins     = hist_bins,
         histtype = "step",
         label    = "Run2 L1 4J15",
         color    = colors[0],
         cumulative = -1,
)
rate_save["x"] = np.array((edge[1:] + edge[:-1])/2).tolist()
rate_save["Run2 L1 4J15"] = np.array(hist).tolist()

hist, edge, _ = ax.hist(np.array([(np.sort(n)[::-1])[3] for n in df["gBlocksEt_wRho"][df["trig_L1_4J15_wRho"]==1]]),
         weights  = df["weight"][df["trig_L1_4J15_wRho"]==1]*luminosity,
         bins     = hist_bins,
         histtype = "step",
         label    = "Raw sum w/ $\\rho$Asub",
         color    = colors[1],
         cumulative = -1,
)
rate_save["Raw sum w/ $\\rho$Asub"] = np.array(hist).tolist()

for i,model in enumerate(args.models_to_plot):
    print("-- Now working on {} model...".format(model))

    hist, edge, _ = ax.hist(np.array([(np.sort(n)[::-1])[3] for n in df["gBlocksEt_{}".format(model)][df["{}_{}".format(args.trigger, model)]==1]]),
             weights  = df["weight"][df["{}_{}".format(args.trigger, model)]==1]*luminosity,
             bins     = hist_bins,
             histtype = "step",
             label    = model,
             color    = colors[2+i],
             cumulative = -1,
    )
    rate_save[model] = np.array(hist).tolist()

    plot_name = plot_name + "__" + model

ax.axvline(x=15, color="black", linestyle="--")
ax.text(0.95, 0.95,"Inst. Luminosity: 2.0 $\\times$ ${10}^{34}$ cm${}^{-2}$ s${}^{-1}$",
     horizontalalignment="right",
     verticalalignment="center",
     transform = ax.transAxes)
ax.text(0.95, 0.91,"<$\mu$> $\leq$ 56.0",
     horizontalalignment="right",
     verticalalignment="center",
     transform = ax.transAxes)
tform = blended_transform_factory(ax.transData, ax.transAxes)
ax.text(14, 0.05,"15 GeV",
     horizontalalignment="right",
     verticalalignment="bottom",
     rotation="vertical",
     color="black",
     transform=tform)
ax.set_yscale('log')
ax.set_xlabel("4th leading L1 reco jet $p_T$ threshold [GeV]")
ax.set_ylabel("Rate [kHz]")
ax.set_title("Cumulative Trigger Rates")
ax.legend(bbox_to_anchor=[0.97, 0.90], loc="upper right")
fig.savefig("plots/{}.png".format(plot_name))

json.dump(rate_save, open("plots/rate.json", 'w'))

print("-- Finished!")
