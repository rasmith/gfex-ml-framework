#Load in modules and functions
import numpy as np
import pandas as pd
import argparse

import os
import subprocess
import time
import shutil

#Arguments passed by the user
parser = argparse.ArgumentParser(description='Split input file, make trigger variables, and then put back together')
parser.add_argument('-f', '--file', dest='ifile', type=str, default=None, help='Single pickle file to run on')
parser.add_argument('--split_no', dest='split_no', type=int, default=None, help='Split dataframe into # chunks')
parser.add_argument('--dir', dest='dir', type=str, default='samples', help='Directory to save processed file')
args = parser.parse_args()

print("Make the new 4J15 trigger for all available models for sample '{}'".format(args.ifile))

#Get directory
dir = args.dir
dir = dir.splitlines()[0]

#Make workspace
print("-- Making workspace...")
if not os.path.exists("workspace"):
    os.makedirs("workspace")

#Read in dataframe
print("-- Opening sample...")
df = pd.read_pickle("{}/{}".format(dir, args.ifile))
file_name = args.ifile
file_name = file_name.replace(".pkl","")

#Split dataframe into chunks
print("-- Splitting sample into {} chunks...".format(args.split_no))
df_chunks = np.array_split(df, args.split_no)
for i,chunk in enumerate(df_chunks):
    #Save chunks in workspace
    chunk.to_pickle("workspace/{}_CHUNK_{}.pkl".format(file_name, i))
    #Submit chunks to queue
    print("Submiting CHUNK #{}".format(i))
    os.system("LSB_JOB_REPORT_MAIL=N bsub -N -W 50:00 -q atlas-t3 'python triggerMake.py --f workspace/{}_CHUNK_{}.pkl'".format(file_name, i))

#Check if the jobs are done
not_done = True
while not_done:
    time.sleep(10) #Wait 10 seconds
    result = subprocess.Popen(["bjobs"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    result = (result.stdout.read()).decode("utf-8")
    os.system("bjobs")
    #Continue once all the jobs are done
    if "No unfinished job found" in result:
        not_done = False

#Put chunks back together
print("-- Putting chunks back together...")
new_df = pd.read_pickle("workspace/{}_PROCESSED_CHUNK_{}.pkl".format(file_name, 0))
for i in range(len(df_chunks))[1:]:
    if i%10==0: print("Adding PROCESSED_CHUNK {}".format(i))
    new_df = new_df.append(pd.read_pickle("workspace/{}_PROCESSED_CHUNK_{}.pkl".format(file_name, i)), ignore_index=True)

print("-- Save new pickle file with added trigger variables...")
new_df.to_pickle("{}/trig_L1_4J15_{}.pkl".format(dir, file_name))

print("-- Deleting workspace...")
shutil.rmtree("workspace")
#os.remove("log.log")

print("-- Finished!")
